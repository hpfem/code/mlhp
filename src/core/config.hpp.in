// This file is part of the mlhp project. License: See LICENSE

#ifndef MLHP_CONFIG_HPP
#define MLHP_CONFIG_HPP

#include <cstddef>
#include <cstdint>
#include <string_view>

#cmakedefine MLHP_DEBUG_CHECKS
#cmakedefine MLHP_MULTITHREADING_OMP

// E.g.: MLHP_INSTANTIATE_DIM(2) MLHP_INSTANTIATE_DIM(3) MLHP_INSTANTIATE_DIM(4)
#define MLHP_DIMENSIONS_XMACRO_LIST ${MLHP_DIMENSIONS_XMACRO_LIST}
#define MLHP_POSTPROCESSING_DIMENSIONS_XMACRO_LIST ${MLHP_POSTPROCESSING_DIMENSIONS_XMACRO_LIST}

namespace mlhp
{
namespace config
{

static constexpr const char* commitId = "${MLHP_COMMIT_ID}";
static constexpr const char* osName = "${CMAKE_SYSTEM_NAME}";
static constexpr const char* osVersion = "${CMAKE_SYSTEM_VERSION}";
static constexpr const char* architecture = "${CMAKE_SYSTEM_PROCESSOR}";
static constexpr const char* compilerId = "${CMAKE_CXX_COMPILER_ID}";
static constexpr const char* compilerVersion = "${CMAKE_CXX_COMPILER_VERSION}";
static constexpr const char* threading = "${MLHP_MULTITHREADING}";
static constexpr std::size_t maxdim = ${MLHP_DIMENSIONS};
static constexpr std::size_t maxdegree = 250;
static constexpr std::size_t simdAlignment = ${MLHP_SIMD_ALIGNMENT};
static constexpr bool debugChecks = "${MLHP_DEBUG_CHECKS}" == std::string_view( "ON" );

} // namespace config

using CellIndex = std::uint${MLHP_INDEX_SIZE_CELLS}_t;
using DofIndex = std::uint${MLHP_INDEX_SIZE_DOFS}_t;

} // namespace mlhp

#endif // MLHP_CONFIG_HPP
