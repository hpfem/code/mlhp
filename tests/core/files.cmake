# This file is part of the mlhp project. License: See LICENSE

set( MLHP_CORE_TEST_SOURCES 
     algorithm_test.cpp 
     assembly_test.cpp 
     basis_test.cpp 
     basisfunctions_test.cpp
     boundary_test.cpp 
     core_test.hpp
     dense_test.cpp 
     implicit_test.cpp 
     integrands_test.cpp 
     kdtree_test.cpp
     main_test.cpp 
     mapping_test.cpp 
     mesh_test.cpp
     ndarray_test.cpp 
     numeric_test.cpp
     partitioning_test.cpp 
     projection_test.cpp 
     quadrature_test.cpp
     singleBaseCell_2D.cpp 
     singleBaseCell_2D.hpp 
     spatialfunctions_test.cpp
     sparse_test.cpp
     testCases_test.cpp 
     triangulation_test.cpp 
     utilities_test.cpp 
)